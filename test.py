#!/usr/bin/python
import unittest
import requests
import json
from get_name_jokes import get_random_name, get_random_joke, output_joke

class TestWebService(unittest.TestCase):

    def test_get_name(self): 
       result = get_random_name().status_code
       self.assertEqual(result, 200, "should be 200 GET call passed")

    def test_get_joke(self):
       result = get_random_joke().status_code
       self.assertEqual(result, 200, "should be 200 GET call passed")
   
    def test_output_joke(self):
       result = type(output_joke())
       self.assertEqual(result, unicode,  "output should be unicode")

    def test_web_service(self):
       result = requests.get("http://127.0.0.1/").status_code
       self.assertEqual(result, 200, "should be 200 GET call passed")


if __name__ == '__main__':
    unittest.main()

