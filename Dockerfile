FROM python:2.7-slim
# Install app dependencies
RUN pip install flask
RUN pip install requests

# Bundle app source
COPY get_name_jokes.py /src/get_name_jokes.py
COPY run_web_service.py /src/run_web_service.py
EXPOSE  80
CMD ["python", "/src/run_web_service.py", "-p 80"]


