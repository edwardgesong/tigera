import requests
import json
import HTMLParser

def get_random_name():
    response = requests.get("http://uinames.com/api/", verify=False)

    if response.status_code == 200:
        return response
    else:
        return None

def get_random_joke():
    response = requests.get("http://api.icndb.com/jokes/random?firstName=John&lastName=Doe&limitTo=[nerdy]", verify=False)

    if response.status_code == 200:
        return response
    else:
        return None

def output_joke():
    name = get_random_name()
    name_json = json.loads(name.content.decode('utf-8'))
    full_name = name_json["name"]+" "+name_json["surname"]

    joke = get_random_joke()
    joke_json = json.loads(joke.content.decode('utf-8'))
    joke = joke_json["value"]["joke"]
    joke = HTMLParser.HTMLParser().unescape(joke) 
 
    return (full_name +" tells a joke: " + joke)

if __name__ == '__main__':
   print(output_joke())
   

